package org.batsunovFirstProg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

public class FileWriter {


    public void writeNumberToFileFromString(String str, String directory) {
        java.io.FileWriter writer = null;


        Pattern pattern = Pattern.compile("(\\d+)");
        List<String> listFoundPattern = new ArrayList<>();

        Scanner s = new Scanner(str);
        s.findAll(pattern)
                .map(MatchResult::group)
                .forEach(el -> listFoundPattern.add(el));

        try {
            writer = new java.io.FileWriter(directory);

            for (String el : listFoundPattern) {
                writer.write(el + " ");
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
