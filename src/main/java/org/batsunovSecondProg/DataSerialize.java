package org.batsunovSecondProg;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class DataSerialize {

    public void serialize(List<Data> dataList, String FileName) throws IOException {

        FileOutputStream FileOutputStream = null;
        ObjectOutputStream objectOutputStream = null;

        try {
            FileOutputStream = new FileOutputStream(FileName);
            objectOutputStream = new ObjectOutputStream(FileOutputStream);

            for (Data data : dataList) {
                objectOutputStream.writeObject(data);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (objectOutputStream != null) {
                objectOutputStream.close();
            }
            if (FileOutputStream != null) {
                FileOutputStream.close();
            }
        }

    }


}
