package org.batsunovSecondProg;

/**
 * !!! Десериализация возможна только 3-х объектов !!!
 */

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class DataDeserialize {

    private List<Data> dataDeserialize = new ArrayList<>();

    public List<Data> deserialize(String FileName) throws IOException, ClassNotFoundException {

        FileInputStream fileInputStream = null;
        ObjectInputStream objectInputStream = null;

        try {
            fileInputStream = new FileInputStream(FileName);
            objectInputStream = new ObjectInputStream(fileInputStream);

            //Этот алгоритм видел в интернете и в презентации,к сожалению не работает,в интернете искал как читать пока не закончатсья
            //там просто огромные алгоритмы.Я знаю,что ограничиваться 3 проходами в цикле фор это очень глупо,но не нашел как сделать по-другоому
            //если вы знаете как реализовать проход пока не закончаться объекты,напишите цикл,пожалуйста
//            while (objectInputStream.read() != -1) {
//                dataDeserialize.add((Data) objectInputStream.readObject());
//            }

            for (int i = 0; i < 3; i++) {
                dataDeserialize.add((Data) objectInputStream.readObject());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (objectInputStream != null) {
                objectInputStream.close();
            }
            if (fileInputStream != null) {
                fileInputStream.close();
            }
        }

        return dataDeserialize;
    }


    public void printDeserializeData() {
        dataDeserialize.stream().forEach(System.out::println);
    }

}

