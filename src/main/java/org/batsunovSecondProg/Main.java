package org.batsunovSecondProg;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Main {


    public static void main(String[] args) throws IOException, ClassNotFoundException {
        final String FName = "fileForSerialize.txt";
        List<Data> dataList = new ArrayList<>(dataProvider());


        System.out.println("Объекты перед сериализацией:");
        dataList.stream().forEach(System.out::println);

        //Serialize
        DataSerialize dataSerialize = new DataSerialize();
        dataSerialize.serialize(dataList,FName);


        //Deserialize
        DataDeserialize dataDeserialize = new DataDeserialize();
        dataDeserialize.deserialize(FName);


        System.out.println("Десериализованые объекты:");
        dataDeserialize.printDeserializeData();

    }


    public static List<Data> dataProvider() {
        return List.of(
                new Data(12L, "Dima", LocalDateTime.of(2020, 12, 1, 2, 5)),
                new Data(1L, "Vanya", LocalDateTime.of(2020, 11, 5, 4, 1)),
                new Data(10L, "Sergey", LocalDateTime.of(2020, 10, 2, 1, 10))
        );
    }
}
