package org.batsunovSecondProg;

import org.junit.Before;
import org.junit.Test;


import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class DataSerializeAndDeserializeTest {
    private final String FName = "fileForSerialize.txt";

    private DataSerialize dataSerialize;
    private DataDeserialize deserializeData;

    private List<Data> dataList;

    @Before
    public void setUp() throws Exception {
        dataSerialize = new DataSerialize();
        deserializeData = new DataDeserialize();

        dataList = new ArrayList<>();
        dataList.add(new Data(18L, "Dima", LocalDateTime.of(2020, 1, 4, 5, 1)));
        dataList.add(new Data(18L, "Dima", LocalDateTime.of(2021, 2, 5, 6, 7)));
        dataList.add(new Data(18L, "Dima", LocalDateTime.of(2010, 1, 1, 2, 6)));
    }

    @Test
    public void shouldReturnTheSameList() throws IOException, ClassNotFoundException {
        List<Data> actualData;

        //Serialize
        dataSerialize.serialize(dataList, FName);

        //Deserialize
        actualData = deserializeData.deserialize(FName);


        assertEquals(dataList, actualData);

    }
}