package org.batsunovFirstProg;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class FileWriterTest {
    @Test
    public void shouldWriteNumberToFile() throws IOException {
        String text = "В класс вошло 10 человек,а вышло 12.И что с этим делать остальным 10?";
        String expected = "10 12 10 ";
        File tempFile = File.createTempFile("test", ".txt");
        FileWriter writer = new FileWriter();

        writer.writeNumberToFileFromString(text, tempFile.getAbsolutePath());
        String actualValue = new String(Files.readAllBytes(Paths.get(tempFile.getAbsolutePath())));


        assertEquals(expected, actualValue);
    }
}